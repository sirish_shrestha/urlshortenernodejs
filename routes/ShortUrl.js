const path = require("path");
module.exports = function(app){
	app.get("/", function(req, res) {
		res.sendFile(path.resolve("./views/index.html"));//Resolve absolute path of the file
	});

};