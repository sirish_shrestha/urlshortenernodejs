//Connect to MongoDB cluster created in http://scalegrid.io
const mongoose = require("mongoose");
const mongoURI = "mongodb://admin:kTWP0WcUCVYeHVl3@SG-URLShortenerDb-18215.servers.mongodirector.com:27017/admin";
const connectOptions = {
	useNewUrlParser: true,
	keepAlive: true,
	reconnectTries: 30
};
//Connect to MongoDB
mongoose.connect(mongoURI, connectOptions, (err, db) =>
{
	if (err) console.log("Error", err);
	console.log("Connected to MongoDB");
});
module.exports = mongoose;