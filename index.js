const express = require("express");
const bodyParser = require('body-parser');
var db = require("./config/db");
const models = require("./models/Schema");
const btoa = require('btoa');
const atob = require('atob');

const app = express();
require("./routes/ShortUrl")(app);

// ExpressJS middleware for serving static JS/CSS files
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
	extended: true
}));

// API for redirection
app.get('/:code', function(req, res) {
	var baseid = req.params.code;
	if(baseid) {
		var id = atob(baseid);
		models.URL.findOne({ _id: id }, function(err, doc) {
			if(doc) {
				//ID found in DB. Redirect to Original URL.
				res.redirect(doc.url);
			} else {
				//ID not found in DB. Redirect to homepage.
				res.redirect('/');
			}
		});
	}
});

// API for shortening
app.post('/shorten', function(req, res, next) {
	var urlData = req.body.url;
	models.URL.findOne({url: urlData}, function(err, doc) {
		if(doc) {
			//IF URL already exists in DB
			res.send({
				url: urlData,
				hash: btoa(doc._id),
				status: 200,
				statusTxt: 'OK'
			});
		} else {
			//IF URL does not exist n DB, create new document in DB.
			var url = new models.URL({
				url: urlData
			});
			url.save(function(err) {
				if(err) {
					return console.error(err);
				}
				res.send({
					url: urlData,
					hash: btoa(url._id),
					status: 200,
					statusTxt: 'OK'
				});
			});
		}
	});
});

app.listen("3000",() => {
	console.log("Server started on port 3000");
});