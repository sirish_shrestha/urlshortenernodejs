var init = (function(){
	$("#form_url_shortener").on("submit", function(e) {
		e.preventDefault();
		var url = $.trim($("#txt_url").val());
		if(url == ""){
			$(".shortened-url").html("Please enter a valid URL to shorten.");
			$("#shorten_area").show();
		}else {
			$.ajax({
				url: "/shorten",
				type: "POST",
				data: {
					url: url
				},
				success: function (data) {
					var _buildUrl = window.location.origin + "/" + data.hash;
					$(".shortened-url").html("<a href=\"" + _buildUrl + "\" target=\"_blank\">" + _buildUrl + "</a>");
					$("#shorten_area").show();
				}
			});
		}
	});
})();




