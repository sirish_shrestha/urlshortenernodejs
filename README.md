## About URL SHORTENER

URL Shortener is a simple fixed length short URL genrerator for a long URL created in nodeJS and Mongo DB. (Database hosted in Scalegrid.io)

## SYSTEM REQUIREMENTS
- node: v10.15.0
- npm: 6.7.0
- Mongo 4.0.3 (hosted in scalegrid.io)

## Dependencies
- mongoose, express, body-parser, atob, btoa

## INSTALLATION / SETUP

```
1. Clone the repository: 
git clone https://sirish_shrestha@bitbucket.org/sirish_shrestha/urlshortenernodejs.git

2. Install dependencies
RUN> npm install

3. Start the project.
npm start

4. Browse:
http://localhost:3000
```