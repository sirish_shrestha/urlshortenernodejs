const mongoose = require("mongoose");
mongoose.set('useFindAndModify', false);
const Schema = mongoose.Schema;

//Counter Collection Schema
const counterSchema  = new Schema({
	_id: { type: String, required: true },
	count: { type: Number, default: 0 }
});
const Counter = mongoose.model('Counter', counterSchema);

// URL Collection Schema
const urlSchema = new Schema({
	_id: {type: Number},
	url: '',
	created_at: ''
});

//Increment the counter in the Counter Collection and set this value as unique id for URL collection.
urlSchema.pre('save', function(next) {
	console.log('APP: Running pre-save');
	var self = this;
	Counter.findByIdAndUpdate({ _id: 'url_count' }, { $inc: { count: 1 } },  { new: true, upsert: true }, function(err, counter) {
		if(err) {
			console.error('APP: Error while finding and updating counter value');
			return next(err)
		};
		self._id = counter.count;
		self.created_at = new Date();
		next();
	});
});
const URL = mongoose.model('URL', urlSchema);

module.exports = {
	Counter: Counter,
	URL: URL,
};